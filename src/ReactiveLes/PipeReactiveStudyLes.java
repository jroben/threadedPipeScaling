// STAR-CCM+ macro: CouetteCell.java
// Written by STAR-CCM+ 14.06.013
package macro;

import java.util.*;
import java.io.*;
import java.nio.file.*;

import star.common.*;
import star.base.neo.*;
import star.base.report.*;

import star.meshing.*;
import star.prismmesher.*;
import star.solidmesher.*;
import star.dualmesher.*;
import star.resurfacer.*;
import star.passivescalar.*;

import star.kwturb.*;
import star.lesturb.*;
import star.rsturb.*;
import star.keturb.*;

import star.passivescalar.*;
import star.segregatedenergy.*;
import star.segregatedflow.*;
import star.walldistance.*;
import star.material.*;
import star.turbulence.*;
import star.flow.*;
import star.metrics.*;
import star.vis.*;

// Simulation file note:
// Reactive Flow Demo
public class PipeReactiveStudyLes extends StarMacro {

        // Wall Reaction Fitting Parameters
        // k_wall = (kn + min(1, delta/delta1) * (kmax - kn)) * k1
        // k_bulk = R_fit_pA * SI^2 + R_fit_pB * SI
        //
        // ScaleTypeIndicator = max(0,min(1, dot(grad(${BariteScale}),
        // -grad(${WallDistance}))/${deltan} + (${WallDistance} > ${WallLayerThickness}
        // ? 0 : 1 )))
        double delta1 = 1E-5; // Vary for the amount of scale needed to reach max reaction rate
        double kn = __kn__; // Vary for nucleation rate as pct of max reaction rate
        double k1 = __k1__; // Max wall reaction rate
        double kmax = 1; // Keep this set to unity
        double deltan = 1e-3; // Used to control the switching between bulk and wall reaction -> see
                              // ScaleTypeIndicator Field Function
        double WallLayerThickness = 1e-6; // Used to control the switching between bulk and wall reaction -> see
        // ScaleTypeIndicator Field Function

        double R_fit_pA = 1.03E-4; // Bulk reaction rate fit
        double R_fit_pB = 0.00822; // Bulk reaction rate fit

        // Fluid Properties
        double density = 997.561; // kg/m3
        double viscosity = 8.8871E-4; // Pa s

        // Concentration
        double concentrationNaSO4 = __concentrationNa2SO4__; // mol/kgw
        double concentrationBaCl2 = __concentrationBaCl2__; // mol/kgw
        String[] scalars = { "mSO4_2-", "mNa_1+", "mBa_2+", "mCl_1-", "BariteSolid", "BariteParticles" };
        boolean[] noDiffusion = { false, false, false, false, true, true };
        boolean[] noConvection = { false, false, false, false, true, false };

        double D_SO4 = 2.0 * 1.06e-5 * 1e-4;
        double D_Na = 1.0 * 1.334e-5 * 1e-4;
        double D_Ba = 2.0 * 0.847e-5 * 1e-4;
        double D_Cl = 1.0 * 2.032e-5 * 1e-4;
        double molarMassH2O = 18.01528e-3; // kg / mole
        double molarMassBarite = 233.39e-3; // kg / mole
        double DensityBarite = 4480; // kg / m3

        // Turbulent Schmidt Number
        double TurbulentSchmidtNumber = __TurbulentSchmidtNumber__;

        // Temperature
        double Temperature = __Temperature__; // C

        // Time-Stepping
        boolean IsSteady = false;

        // TurbulenceModelling
        String TurbulenceModel = "LES"; // "RANS-kOmega" / "LES" / "Laminar" / "RANS-RST" / "RANS-kEpsilon"

        // New FileName
        String uid = UUID.randomUUID().toString().substring(0, 5);
        String SimName = "Sim_" + TurbulenceModel + "_" + uid + ".sim";
        String SessionDirectory = "";

        int iterationUpdateFrequency = 5;
        int innerIterations = 1;

        public void execute() {
                Simulation simulation = getActiveSimulation();
                SessionDirectory = simulation.getSessionDir();
                StartLesReactiveSimulation();

        }

        private void StartLesReactiveSimulation() {

                swapToUnsteady();
                SimName = "Sim_" + TurbulenceModel + "_REACTIVE_LES_" + uid + ".sim";

                enableSecondOrderTimestep();
                setInnerIterationCount(innerIterations);
                setMinWallDistance(Math.min(WallLayerThickness / 5, 1e-6));

                // Enable passive scalars and static temp
                EnableSpecies();

                // addIsoThermal(Temperature);
                setTemperature(Temperature);
                SetScalarSchmidtNumber("mNa_1+", viscosity / (D_Na * density));
                SetScalarSchmidtNumber("mSO4_2-", viscosity / (D_SO4 * density));
                SetScalarSchmidtNumber("mBa_2+", viscosity / (D_Ba * density));
                SetScalarSchmidtNumber("mCl_1-", viscosity / (D_Cl * density));
                for (int i = 0; i < scalars.length - 2; i++) { // -2 becuse excluding the solid barite and particles
                        SetScalarTurbulentSchmidtNumber(scalars[i], TurbulentSchmidtNumber);
                }

                SetScalarInitialConcentration(0, concentrationNaSO4 * 1 * 0.5);
                SetScalarInitialConcentration(1, concentrationNaSO4 * 2 * 0.5);
                SetScalarInitialConcentration(2, concentrationBaCl2 * 1 * 0.5);
                SetScalarInitialConcentration(3, concentrationBaCl2 * 2 * 0.5);
                SetScalarInitialConcentration(4, 0);
                SetScalarInitialConcentration(5, 0);

                // Reactive part
                CreateSpeciesFieldFunctions();
                AddUserLib(SessionDirectory + "/libuser.so");

                // Reaction Functions
                Dimensions dimensionless = Dimensions.Builder().build();
                Dimensions length = Dimensions.Builder().length(1).build();
                Dimensions velocity = Dimensions.Builder().length(1).time(-1).build();
                Dimensions velocitySquared = Dimensions.Builder().length(2).time(-2).build();
                Dimensions diffusivity = Dimensions.Builder().length(2).time(-1).build();
                createScalarFieldFunction("Saturation Rate", "SR", "pow(10,${UserPitzerSaturationIndex})",
                                dimensionless);
                CreateGlobalDimensionlessParameter("R_fit_pA", "" + R_fit_pA);
                CreateGlobalDimensionlessParameter("R_fit_pB", "" + R_fit_pB);
                CreateGlobalDimensionlessParameter("k1", "" + k1);
                CreateGlobalDimensionlessParameter("kn", "" + kn);
                CreateGlobalDimensionlessParameter("kmax", "" + kmax);
                CreateGlobalDimensionlessParameter("delta1", "" + delta1);
                CreateGlobalDimensionlessParameter("deltan", "" + deltan);
                // CreateGlobalDimensionlessParameter("Temperature", "" + Temperature + " +
                // 273.15");
                CreateGlobalDimensionlessParameter("WallLayerThickness", "" + WallLayerThickness);

                createScalarFieldFunction("isBulk", "isBulk", "${WallDistance} > ${WallLayerThickness} ? 1 : 0 ",
                                dimensionless);
                createScalarFieldFunction("isCell", "isCell", "$${Position}[1] > 0 && $${Position}[1] > 0.030 ? 1 : 0 ",
                                dimensionless);
                // ScaleTypeIndicator used to differentiate between precipitation (0) and
                // deposisiton (1)
                createScalarFieldFunction("Scale Type Indicator", "ScaleTypeIndicator",
                                "max(0,min(1, dot(grad(${BariteSolid}), grad(${WallDistance}))/${deltan}/mag(grad(${WallDistance})) + (${WallDistance} > ${WallLayerThickness} ? 0 : 1 )))",
                                dimensionless);

                // Bulk reaction rate model
                createScalarFieldFunction("Bulk Reaction Rate Constant", "K_bulk",
                                "${R_fit_pA}*pow(${SR},2) + ${R_fit_pB} * ${SR} * (1-${ScaleTypeIndicator})",
                                dimensionless);
                createScalarFieldFunction("Bulk Reaction Rate", "R_Bulk",
                                "${K_bulk}*${mBa_2+}*${mSO4_2-}*max(0,${SR}-1)", dimensionless);
                createScalarFieldFunction("Bulk R_Ba", "R_Ba_Bulk", "-${R_Bulk}", dimensionless);
                createScalarFieldFunction("Bulk R_SO4", "R_SO4_Bulk", "-${R_Bulk}", dimensionless);
                createScalarFieldFunction("Bulk R_BaSO4", "R_BaSO4_Bulk", "${R_Bulk}", dimensionless);

                createScalarFieldFunction("Bulk dRdmBa", "dRdmBa_Bulk",
                                "-0.5*${mBa_2+}*${K_bulk}*pow(${UserPitzerActivityCoefficient},2)/${UserEquilibriumConstant}",
                                dimensionless);
                createScalarFieldFunction("Bulk dRdmSO4", "dRdmSO4_Bulk",
                                "-0.5*${mSO4_2-}*${K_bulk}*pow(${UserPitzerActivityCoefficient},2)/${UserEquilibriumConstant}",
                                dimensionless);
                createScalarFieldFunction("Bulk dRdmBaSO4", "dRdmBaSO4_Bulk", "${dRdmSO4_Bulk} + ${dRdmBa_Bulk}",
                                dimensionless);

                // Wall reaction rate model
                createScalarFieldFunction("Wall Scale Thickness", "deltaBaSO4",
                                "${BariteSolid} * ${Volume} / (pow(${Volume},2.0/3.0)*${MolarMassBarite}*${DensityBarite})",
                                length); // A= Volume^(2/3)is bad approximation but cant get wall-area
                createScalarFieldFunction("Wall Reaction Rate Constant", "K_wall",
                                "${k1} * (${kn} + min(1.0, ${deltaBaSO4}/${delta1}) * (${kmax}-${kn})) *(${ScaleTypeIndicator}) ",
                                dimensionless);
                createScalarFieldFunction("Wall Reaction Rate", "R_Wall",
                                "${K_wall}*${mBa_2+}*${mSO4_2-}*max(0,${SR}-1)", dimensionless);
                createScalarFieldFunction("Wall R_Ba", "R_Ba_Wall", "-${R_Wall}", dimensionless);
                createScalarFieldFunction("Wall R_SO4", "R_SO4_Wall", "-${R_Wall}", dimensionless);
                createScalarFieldFunction("Wall R_BaSO4", "R_BaSO4_Wall", "${R_Wall}", dimensionless);

                createScalarFieldFunction("Wall dRdmBa", "dRdmBa_Wall",
                                "-0.5*${mBa_2+}*${K_wall}*pow(${UserPitzerActivityCoefficient},2)/${UserEquilibriumConstant}",
                                dimensionless);
                createScalarFieldFunction("Wall dRdmSO4", "dRdmSO4_Wall",
                                "-0.5*${mSO4_2-}*${K_wall}*pow(${UserPitzerActivityCoefficient},2)/${UserEquilibriumConstant}",
                                dimensionless);
                createScalarFieldFunction("Wall dRdmBaSO4", "dRdmBaSO4_Wall", "${dRdmBa_Wall} + ${dRdmSO4_Wall}",
                                dimensionless);

                // Source Functions and derivatives
                createScalarFieldFunction("Source R_Ba", "R_Ba_Source", "${R_Ba_Wall} + ${R_Ba_Bulk}", dimensionless);
                createScalarFieldFunction("Source R_SO4", "R_SO4_Source", "${R_SO4_Wall} + ${R_SO4_Bulk}",
                                dimensionless);
                createScalarFieldFunction("Source R_BariteScale", "R_BariteScale_Source", "${R_BaSO4_Wall}",
                                dimensionless);
                createScalarFieldFunction("Source R_BariteParticles", "R_BariteParticles_Source", "${R_BaSO4_Bulk}",
                                dimensionless);

                createScalarFieldFunction("Source dRdmBa", "dRdmBa_Source", "0*(${dRdmBa_Bulk}+${dRdmBa_Wall})",
                                dimensionless);
                createScalarFieldFunction("Source dRdmSO4", "dRdmSO4_Source", "0*(${dRdmSO4_Bulk}+${dRdmSO4_Wall})",
                                dimensionless);
                createScalarFieldFunction("Source dRdmBariteScale", "dRdmBariteScale_Source", "0*(${dRdmBaSO4_Wall})",
                                dimensionless);
                createScalarFieldFunction("Source dRdmBariteParticles", "dRdmBariteParticles_Source",
                                "0*(${dRdmBaSO4_Bulk})", dimensionless);

                String mode = "wall";

                // Reactive Source Terms
                if (mode == "wall") {
                        SetPassiveScalarWallFlux(0, "R_SO4_Wall", "dRdmSO4_Source"); // Sulphate Consumption
                        SetPassiveScalarWallFlux(2, "R_Ba_Wall", "dRdmBa_Source"); // Barium Consumption
                        SetPassiveScalarWallFlux(4, "R_BaSO4_Wall", "dRdmBariteScale_Source"); // Deposition

                        SetPassiveScalarMassSource(0, "R_SO4_Bulk", "dRdmSO4_Source"); // Sulphate Consumption
                        SetPassiveScalarMassSource(2, "R_Ba_Bulk", "dRdmBa_Source"); // Barium Consumption
                        SetPassiveScalarMassSource(5, "R_BariteParticles_Source", "dRdmBariteScale_Source"); // Precipitation
                        ChangeMeanDepositionFunction("R_BaSO4_Wall");
                } else {
                        SetPassiveScalarMassSource(0, "R_SO4_Source", "dRdmSO4_Bulk"); // Sulphate Consumption
                        SetPassiveScalarMassSource(2, "R_Ba_Source", "dRdmBa_Bulk"); // Barium Consumption
                        SetPassiveScalarMassSource(4, "R_BariteScale_Source", "dRdmBariteScale_Source"); // Deposition
                        SetPassiveScalarMassSource(5, "R_BariteParticles_Source", "dRdmBariteParticles_Source"); // Precipitation
                        ChangeMeanDepositionFunction("R_BaSO4_Wall");
                }

                // Effluent tracking
                for (int i = 0; i < scalars.length; i++) {
                        RecordOutletConcentrations(scalars[i]);
                }
                RecordOutletConcentrations("SR");
                setOutletBackflowConcentration(scalars);

                createScalarFieldFunction("Unity", "Unity", "1", dimensionless);
                setPorosityFieldFunction("Unity");

                // Save and prepare
                InitializeSolution();
                SetAutoSave();
                Save();

                RunTime(1e-4);
                // Porosity
                createScalarFieldFunction("BaritePorosity", "BaritePorosity", "1-${BariteScaleVolumeFraction}",
                                dimensionless);
                setPorosityFieldFunction("BaritePorosity");
                
                RunTime(4);

        }

        private void Save() {
                Simulation simulation = getActiveSimulation();

                String fullPath = SessionDirectory + "/Results/" + SimName;

                try {
                        Files.createDirectories(Paths.get(SessionDirectory + "/Results"));
                        simulation.saveState(fullPath);
                } catch (Exception ex) {
                        simulation.println(ex);
                }
        }

        private void RecordOutletConcentrations(String Scalar) {

                Simulation simulation = getActiveSimulation();

                MassFlowAverageReport massFlowAverageReport = simulation.getReportManager()
                                .createReport(MassFlowAverageReport.class);

                FieldFunction primitiveFieldFunction = simulation.getFieldFunctionManager().getFunction(Scalar);

                massFlowAverageReport.setFieldFunction(primitiveFieldFunction);

                massFlowAverageReport.setPresentationName("Mass Flow Averaged Outlet Concentration of " + Scalar);

                Region region = simulation.getRegionManager().getRegion("Fluid");
                Boundary boundary = region.getBoundaryManager().getBoundary("Outlet");
                massFlowAverageReport.getParts().setObjects(boundary);

                simulation.getMonitorManager().createMonitorAndPlot(
                                new NeoObjectVector(new Object[] { massFlowAverageReport }), true, "%1$s Plot");

                ReportMonitor reportMonitor = ((ReportMonitor) simulation.getMonitorManager()
                                .getMonitor("Mass Flow Averaged Outlet Concentration of " + Scalar + " Monitor"));

                MonitorPlot monitorPlot_1 = simulation.getPlotManager().createMonitorPlot(
                                new NeoObjectVector(new Object[] { reportMonitor }),
                                "Mass Flow Averaged Outlet Concentration of " + Scalar + " Monitor Plot");
        }

        private void freezeFlow() {

                Simulation simulation = getActiveSimulation();

                SegregatedFlowSolver segregatedFlowSolver = ((SegregatedFlowSolver) simulation.getSolverManager()
                                .getSolver(SegregatedFlowSolver.class));

                segregatedFlowSolver.setFreezeFlow(true);
        }

        private void InitializeSolution() {
                Simulation simulation = getActiveSimulation();

                Solution solution = simulation.getSolution();

                solution.initializeSolution();
                solution.initializeSolution();
        }

        private void freezeRst() {

                Simulation simulation = getActiveSimulation();

                EbRsTurbSolver ebRsTurbSolver = ((EbRsTurbSolver) simulation.getSolverManager()
                                .getSolver(EbRsTurbSolver.class));

                ebRsTurbSolver.setFrozen(true);
        }

        private void createScalarFieldFunction(String name, String identifier, String definition, Dimensions units) {
                Simulation simulation = getActiveSimulation();
                UserFieldFunction userFieldFunction = simulation.getFieldFunctionManager().createFieldFunction();
                userFieldFunction.getTypeOption().setSelected(FieldFunctionTypeOption.Type.SCALAR);
                userFieldFunction.setPresentationName(name);
                userFieldFunction.setFunctionName(identifier);
                userFieldFunction.setDimensions(units);
                userFieldFunction.setDefinition(definition);
        }

        private void EnableAdaptiveTimeStepping(double getTargetMeanCfl, double getTargetMaxCfl) {

                Simulation simulation = getActiveSimulation();
                PhysicsContinuum physicsContinuum = ((PhysicsContinuum) simulation.getContinuumManager()
                                .getContinuum("LES"));
                physicsContinuum.enable(AdaptiveTimeStepModel.class);

                AdaptiveTimeStepModel adaptiveTimeStepModel = physicsContinuum.getModelManager()
                                .getModel(AdaptiveTimeStepModel.class);
                ConvectiveCflTimeStepProvider convectiveCflTimeStepProvider = adaptiveTimeStepModel
                                .getTimeStepProviderManager().createObject(ConvectiveCflTimeStepProvider.class);

                convectiveCflTimeStepProvider.getTargetMeanCfl().setValue(getTargetMeanCfl);
                convectiveCflTimeStepProvider.getTargetMaxCfl().setValue(getTargetMaxCfl);
        }

        private void setOutletBackflowConcentration(String[] scalars) {

                Simulation simulation = getActiveSimulation();
                Region region = simulation.getRegionManager().getRegion("Fluid");
                Boundary boundary = region.getBoundaryManager().getBoundary("Outlet");

                PassiveScalarProfile passiveScalarProfile = boundary.getValues().get(PassiveScalarProfile.class);
                passiveScalarProfile.setMethod(CompositeArrayProfileMethod.class);

                for (int i = 0; i < scalars.length; i++) {
                        ScalarProfile scalarProfile_i = passiveScalarProfile
                                        .getMethod(CompositeArrayProfileMethod.class).getProfile(i);
                        scalarProfile_i.getMethod(ConstantScalarProfileMethod.class).getQuantity().setDefinition(
                                        "${MassFlowAveragedOutletConcentrationof" + scalars[i] + "Report}");
                }

        }

        private void SetScalarInitialConcentration(int no, double concentration) {
                Simulation simulation = getActiveSimulation();
                PhysicsContinuum physicsContinuum = ((PhysicsContinuum) simulation.getContinuumManager()
                                .getContinuum("LES"));
                PassiveScalarProfile passiveScalarProfile = physicsContinuum.getInitialConditions()
                                .get(PassiveScalarProfile.class);
                passiveScalarProfile.setMethod(CompositeArrayProfileMethod.class);
                ScalarProfile scalarProfile = passiveScalarProfile.getMethod(CompositeArrayProfileMethod.class)
                                .getProfile(no);
                scalarProfile.getMethod(ConstantScalarProfileMethod.class).getQuantity().setValue(concentration);
        }

        private void SetWallConcentration(int no, String FieldFunction) {

                Simulation simulation = getActiveSimulation();

                Region region = simulation.getRegionManager().getRegion("Fluid");

                Boundary boundary = region.getBoundaryManager().getBoundary("Wall");

                boundary.getConditions().get(WallPassiveScalarOption.class)
                                .setSelected(WallPassiveScalarOption.Type.SPECIFIED_SCALAR);

                PassiveScalarProfile passiveScalarProfile = boundary.getValues().get(PassiveScalarProfile.class);

                passiveScalarProfile.setMethod(CompositeArrayProfileMethod.class);

                ScalarProfile scalarProfile = passiveScalarProfile.getMethod(CompositeArrayProfileMethod.class)
                                .getProfile(0);

                scalarProfile.setMethod(FunctionScalarProfileMethod.class);

                PrimitiveFieldFunction primitiveFieldFunction = ((PrimitiveFieldFunction) simulation
                                .getFieldFunctionManager().getFunction("UserPitzerDiffModelWallmBa_2+"));

                scalarProfile.getMethod(FunctionScalarProfileMethod.class).setFieldFunction(primitiveFieldFunction);
        }

        private void SetScalarSchmidtNumber(String scalar, double Schmidt) {

                Simulation simulation = getActiveSimulation();

                PhysicsContinuum physicsContinuum = ((PhysicsContinuum) simulation.getContinuumManager()
                                .getContinuum("LES"));

                PassiveScalarModel passiveScalarModel = physicsContinuum.getModelManager()
                                .getModel(PassiveScalarModel.class);

                PassiveScalarMaterial passiveScalarMaterial = ((PassiveScalarMaterial) passiveScalarModel
                                .getPassiveScalarManager().getPassiveScalarMaterial(scalar));

                SchmidtNumberDiffusivityMethod schmidtNumberDiffusivityMethod = ((SchmidtNumberDiffusivityMethod) passiveScalarMaterial
                                .getMaterialProperties().getMaterialProperty(PassiveScalarDiffusivityProperty.class)
                                .getMethod());

                schmidtNumberDiffusivityMethod.setSchmidtNumber(Schmidt);
        }

        private void SetScalarTurbulentSchmidtNumber(String scalar, double Schmidt) {

                Simulation simulation = getActiveSimulation();

                PhysicsContinuum physicsContinuum = ((PhysicsContinuum) simulation.getContinuumManager()
                                .getContinuum("LES"));

                PassiveScalarModel passiveScalarModel = physicsContinuum.getModelManager()
                                .getModel(PassiveScalarModel.class);

                PassiveScalarMaterial passiveScalarMaterial = ((PassiveScalarMaterial) passiveScalarModel
                                .getPassiveScalarManager().getPassiveScalarMaterial(scalar));

                ConstantMaterialPropertyMethod constantMaterialPropertyMethod = ((ConstantMaterialPropertyMethod) passiveScalarMaterial
                                .getMaterialProperties()
                                .getMaterialProperty(PassiveScalarTurbulentSchmidtNumberProperty.class).getMethod());

                constantMaterialPropertyMethod.getQuantity().setValue(Schmidt);
        }

        private void ChangeMeanDepositionFunction(String functionName) {

                Simulation simulation_0 = getActiveSimulation();

                FieldMeanMonitor fieldMeanMonitor_0 = ((FieldMeanMonitor) simulation_0.getMonitorManager()
                                .getMonitor("Mean Deposition Rate"));

                FieldFunction FieldFunction_0 = simulation_0.getFieldFunctionManager()
                                .getFunction(functionName);

                fieldMeanMonitor_0.setFieldFunction(FieldFunction_0);
        }

        private void ExportAllMonitors() {
                Simulation simulation = getActiveSimulation();

                try {
                        String SessionDirectory = simulation.getSessionDir();
                        Files.createDirectories(Paths.get(SessionDirectory + "/monitors"));

                        Collection<Monitor> monitors = simulation.getMonitorManager().getMonitors();

                        for (Monitor monitor : monitors) {
                                monitor.export(SessionDirectory + "/monitors/"
                                                + monitor.getPresentationName().replace(" ", "") + ".csv");
                        }
                } catch (Exception ex) {
                        simulation.println(ex);
                }

        }

        private void SwapToLes() {

                Simulation simulation = getActiveSimulation();
                PhysicsContinuum physicsContinuum = ((PhysicsContinuum) simulation.getContinuumManager()
                                .getContinuum("LES"));

                if (IsSteady) {
                        SteadyModel steadyModel = physicsContinuum.getModelManager().getModel(SteadyModel.class);
                        physicsContinuum.disableModel(steadyModel);
                        physicsContinuum.enable(ImplicitUnsteadyModel.class);
                        IsSteady = false;

                }

                if (TurbulenceModel == "RANS-kOmega") {
                        physicsContinuum.disableModel(
                                        physicsContinuum.getModelManager().getModel(TurbulentModel.class));
                        physicsContinuum.disableModel(
                                        physicsContinuum.getModelManager().getModel(RansTurbulenceModel.class));
                        physicsContinuum.disableModel(
                                        physicsContinuum.getModelManager().getModel(KOmegaTurbulence.class));
                        physicsContinuum.disableModel(
                                        physicsContinuum.getModelManager().getModel(SstKwTurbModel.class));
                        physicsContinuum.disableModel(
                                        physicsContinuum.getModelManager().getModel(KwAllYplusWallTreatment.class));
                        physicsContinuum.disableModel(
                                        physicsContinuum.getModelManager().getModel(GammaTransitionModel.class));

                } else if (TurbulenceModel == "RANS-RST") {
                        physicsContinuum.disableModel(
                                        physicsContinuum.getModelManager().getModel(RansTurbulenceModel.class));
                        physicsContinuum.disableModel(
                                        physicsContinuum.getModelManager().getModel(ReynoldsStressTurbulence.class));
                        physicsContinuum.disableModel(physicsContinuum.getModelManager().getModel(EbRsTurbModel.class));
                        physicsContinuum.disableModel(
                                        physicsContinuum.getModelManager().getModel(EbRsAllYplusWallTreatment.class));

                } else if (TurbulenceModel == "RANS-kEpsilon") {
                        physicsContinuum.disableModel(
                                        physicsContinuum.getModelManager().getModel(RansTurbulenceModel.class));
                        physicsContinuum.disableModel(
                                        physicsContinuum.getModelManager().getModel(KEpsilonTurbulence.class));
                        physicsContinuum.disableModel(
                                        physicsContinuum.getModelManager().getModel(RkeTwoLayerTurbModel.class));
                        physicsContinuum.disableModel(physicsContinuum.getModelManager()
                                        .getModel(KeTwoLayerAllYplusWallTreatment.class));
                } else if (TurbulenceModel == "LES") {

                } else {
                        physicsContinuum.disableModel(physicsContinuum.getModelManager().getModel(LaminarModel.class));
                }

                TurbulenceModel = "LES";
                physicsContinuum.enable(TurbulentModel.class);
                physicsContinuum.enable(LesTurbulenceModel.class);
                physicsContinuum.enable(WaleSgsModel.class);
                physicsContinuum.enable(LesAllYplusWallTreatment.class);

                setLesCw(0.325);
        }

        private void setLesCw(double Cw) {
                Simulation simulation = getActiveSimulation();
                PhysicsContinuum physicsContinuum = ((PhysicsContinuum) simulation.getContinuumManager()
                                .getContinuum("LES"));
                WaleSgsModel waleSgsModel = physicsContinuum.getModelManager().getModel(WaleSgsModel.class);
                waleSgsModel.setCw(Cw);
        }

        private void CreateSpeciesFieldFunctions() {

                CreateGlobalDimensionlessParameter("MolarMassWater", "" + molarMassH2O);
                CreateGlobalDimensionlessParameter("MolarMassBarite", "" + molarMassBarite);
                CreateGlobalDimensionlessParameter("DensityBarite", "" + DensityBarite);

                createDimensionlessFieldFuncs("mEtc_1-", "${mCl_1-} + ${mNa_1+}");
                createDimensionlessFieldFuncs("mEtc_2-", "${mSO4_2-} + ${mBa_2+}");

                createDimensionlessFieldFuncs("mTot", "1.0 / ${MolarMassWater} + ${mEtc_1-} + ${mEtc_2-}");

                createDimensionlessFieldFuncs("yEtc_1-", "${mEtc_1-} / ${mTot}");
                createDimensionlessFieldFuncs("yEtc_2-", "${mEtc_2-} / ${mTot}");

                for (int i = 0; i < scalars.length; i++) {
                        String mName = scalars[i];
                        String yName = mName.replaceFirst("m", "y");
                        String definition = "${" + mName + "} / ${mTot}";
                        createDimensionlessFieldFuncs(yName, definition);
                }

                createDimensionlessFieldFuncs("dBaSO4dt", "${UserDebyeHuckelWallDeposition(Molality)} * ${Density}");
                createDimensionlessFieldFuncs("dydt", "-${UserDebyeHuckelWallDeposition(MoleFraction)} / ${Volume}");
                createDimensionlessFieldFuncs("dBadt", "-${dBaSO4dt}");
                createDimensionlessFieldFuncs("dSO4dt", "-${dBaSO4dt}");

                createDimensionlessFieldFuncs("BariteVolumeFraction",
                                "(${BariteParticles} + ${BariteSolid}) * ${Density} * ${MolarMassBarite} / ${DensityBarite}");

                createDimensionlessFieldFuncs("BariteScaleVolumeFraction",
                                "(${BariteSolid}) * ${Density} * ${MolarMassBarite} / ${DensityBarite}");

        }

        private void EnableSpecies() {
                // Totals:

                // Inlet B280
                double mNa2SO4_inletA = concentrationNaSO4 * 0.5;
                double nu_Na = 2;
                double nu_SO4 = 1;
                double nu_Na2SO4 = 1;
                double mNa_inletA = (nu_Na / nu_Na2SO4) * mNa2SO4_inletA;
                double mSO4_inletA = (nu_SO4 / nu_Na2SO4) * mNa2SO4_inletA;

                // Inlet B
                double mBaCl2_inletB = concentrationBaCl2 * 0.5;
                double nu_Cl = 2;
                double nu_Ba = 1;
                double nu_BaCl2 = 1;
                double mBa_inletB = (nu_Ba / nu_BaCl2) * mBaCl2_inletB;
                double mCl_inletB = (nu_Cl / nu_BaCl2) * mBaCl2_inletB;

                double[] inletConcentrations = { mSO4_inletA, mNa_inletA, mBa_inletB, mCl_inletB, 0.0 };

                AddPassiveScalar(scalars, noDiffusion, noConvection);
                SetPassiveScalarInlet("Inlet", inletConcentrations);
        }

        private void RunSimulation() {

                Simulation simulation = getActiveSimulation();
                simulation.getSimulationIterator().run();
        }

        private void RunTime(double time) {
                Simulation simulation = getActiveSimulation();

                PhysicalTimeStoppingCriterion physicalTimeStoppingCriterion = ((PhysicalTimeStoppingCriterion) simulation
                                .getSolverStoppingCriterionManager()
                                .getSolverStoppingCriterion("Maximum Physical Time"));

                double curTime = simulation.getSolution().getPhysicalTime();
                physicalTimeStoppingCriterion.getMaximumTime().setValue(curTime + time);

                RunSimulation();
        }

        private void setInnerIterationCount(int count) {
                Simulation simulation = getActiveSimulation();
                InnerIterationStoppingCriterion innerIterationStoppingCriterion = ((InnerIterationStoppingCriterion) simulation
                                .getSolverStoppingCriterionManager()
                                .getSolverStoppingCriterion("Maximum Inner Iterations"));
                innerIterationStoppingCriterion.setMaximumNumberInnerIterations(count);
        }

        private void enableSecondOrderTimestep() {
                Simulation simulation = getActiveSimulation();
                ImplicitUnsteadySolver implicitUnsteadySolver = ((ImplicitUnsteadySolver) simulation.getSolverManager()
                                .getSolver(ImplicitUnsteadySolver.class));
                implicitUnsteadySolver.getTimeDiscretizationOption()
                                .setSelected(TimeDiscretizationOption.Type.SECOND_ORDER);
        }

        private void AddUserLib(String libPath) {
                Simulation simulation = getActiveSimulation();
                UserLibrary userLibrary = simulation.getUserFunctionManager().createUserLibrary(resolvePath(libPath));
        }

        private void addIsoThermal(double TempC) {

                Simulation simulation = getActiveSimulation();

                PhysicsContinuum physicsContinuum = ((PhysicsContinuum) simulation.getContinuumManager()
                                .getContinuum("LES"));

                physicsContinuum.enable(SegregatedFluidIsothermalModel.class);

                SegregatedFluidIsothermalModel segregatedFluidIsothermalModel = physicsContinuum.getModelManager()
                                .getModel(SegregatedFluidIsothermalModel.class);

                Units units = ((Units) simulation.getUnitsManager().getObject("C"));

                segregatedFluidIsothermalModel.getContinuumTemperature().setUnits(units);

                segregatedFluidIsothermalModel.getContinuumTemperature().setValue(TempC);
        }

        private void setTemperature(double TempC) {

                Simulation simulation = getActiveSimulation();

                PhysicsContinuum physicsContinuum = ((PhysicsContinuum) simulation.getContinuumManager()
                                .getContinuum("LES"));

                SegregatedFluidIsothermalModel segregatedFluidIsothermalModel = physicsContinuum.getModelManager()
                                .getModel(SegregatedFluidIsothermalModel.class);

                Units units = ((Units) simulation.getUnitsManager().getObject("C"));

                segregatedFluidIsothermalModel.getContinuumTemperature().setUnits(units);

                segregatedFluidIsothermalModel.getContinuumTemperature().setValue(TempC);
        }

        private void AddPassiveScalar(String[] scalars, boolean[] noDiffusion, boolean[] noConvection) {

                Simulation simulation = getActiveSimulation();
                PhysicsContinuum physicsContinuum = ((PhysicsContinuum) simulation.getContinuumManager()
                                .getContinuum("LES"));
                physicsContinuum.enable(PassiveScalarModel.class);

                for (int i = 0; i < scalars.length; i++) {
                        String scalar = scalars[i];
                        PassiveScalarModel passiveScalarModel = physicsContinuum.getModelManager()
                                        .getModel(PassiveScalarModel.class);

                        PassiveScalarMaterial passiveScalarMaterial = passiveScalarModel.getPassiveScalarManager()
                                        .createPassiveScalarMaterial(PassiveScalarMaterial.class);

                        if (noDiffusion[i] && noConvection[i]) {
                                // PassiveScalarTransportOption.Type.NO_CONVECTION_OR_DIFFUSION
                                passiveScalarMaterial.getTransportOption().setSelected(
                                                PassiveScalarTransportOption.Type.NO_CONVECTION_OR_DIFFUSION);
                        } else if (noDiffusion[i] && !noConvection[i]) {
                                // PassiveScalarTransportOption.Type.NO_CONVECTION_OR_DIFFUSION
                                passiveScalarMaterial.getTransportOption()
                                                .setSelected(PassiveScalarTransportOption.Type.CONVECTION_ONLY);
                        } else if (!noDiffusion[i] && noConvection[i]) {
                                // PassiveScalarTransportOption.Type.NO_CONVECTION_OR_DIFFUSION
                                passiveScalarMaterial.getTransportOption()
                                                .setSelected(PassiveScalarTransportOption.Type.DIFFUSION_ONLY);
                        }

                        passiveScalarMaterial.getClipMode().setSelected(PassiveScalarClipMode.Type.CLIP_BOTH);

                        passiveScalarMaterial.setMaxAllowable(1.0);

                        passiveScalarMaterial.setPresentationName(scalar);

                        if (TurbulenceModel == "RANS-RST" && !noDiffusion[i]) {
                                passiveScalarMaterial.setUseGGDH(true);
                        }
                }
        }

        private void SetPassiveScalarInlet(String inletName, double[] inletConcentrations) {

                Simulation simulation = getActiveSimulation();

                Region region = simulation.getRegionManager().getRegion("Fluid");

                Boundary boundary = region.getBoundaryManager().getBoundary(inletName);

                PassiveScalarProfile passiveScalarProfile = boundary.getValues().get(PassiveScalarProfile.class);

                passiveScalarProfile.setMethod(CompositeArrayProfileMethod.class);

                for (int i = 0; i < inletConcentrations.length; i++) {
                        ScalarProfile scalarProfile = passiveScalarProfile.getMethod(CompositeArrayProfileMethod.class)
                                        .getProfile(i);
                        scalarProfile.getMethod(ConstantScalarProfileMethod.class).getQuantity()
                                        .setValue(inletConcentrations[i]);

                }

        }

        private void createDimensionlessFieldFuncs(String name, String definition) {

                Simulation simulation = getActiveSimulation();

                UserFieldFunction userFieldFunction = simulation.getFieldFunctionManager().createFieldFunction();

                userFieldFunction.getTypeOption().setSelected(FieldFunctionTypeOption.Type.SCALAR);

                userFieldFunction.setPresentationName(name);
                userFieldFunction.setFunctionName(name);
                userFieldFunction.setDefinition(definition);

        }

        private void SetNormalStressOption(boolean activated) {

                if (TurbulenceModel == "RANS-kOmega") {
                        Simulation simulation = getActiveSimulation();

                        PhysicsContinuum physicsContinuum = ((PhysicsContinuum) simulation.getContinuumManager()
                                        .getContinuum("LES"));

                        SstKwTurbModel sstKwTurbModel = physicsContinuum.getModelManager()
                                        .getModel(SstKwTurbModel.class);

                        sstKwTurbModel.setNormalStressOption(activated);
                }
        }

        private void AddSurfaceSideViewScene(String name, String fieldFunction, double Min, double Max) {

                Simulation simulation = getActiveSimulation();

                simulation.getSceneManager().createScalarScene("Scalar Scene", "Outline", "Scalar");
                Scene scene_0 = simulation.getSceneManager().getScene("Scalar Scene 1");
                scene_0.initializeAndWait();

                PartDisplayer partDisplayer_0 = ((PartDisplayer) scene_0.getDisplayerManager()
                                .getDisplayer("Outline 1"));
                partDisplayer_0.initialize();

                ScalarDisplayer scalarDisplayer_0 = ((ScalarDisplayer) scene_0.getDisplayerManager()
                                .getDisplayer("Scalar 1"));
                scalarDisplayer_0.initialize();

                Legend legend_0 = scalarDisplayer_0.getLegend();
                BlueRedLookupTable blueRedLookupTable_0 = ((BlueRedLookupTable) simulation.get(LookupTableManager.class)
                                .getObject("blue-red"));
                legend_0.setLookupTable(blueRedLookupTable_0);

                SceneUpdate sceneUpdate_0 = scene_0.getSceneUpdate();
                HardcopyProperties hardcopyProperties_0 = sceneUpdate_0.getHardcopyProperties();
                hardcopyProperties_0.setCurrentResolutionWidth(25);
                hardcopyProperties_0.setCurrentResolutionHeight(25);
                hardcopyProperties_0.setCurrentResolutionWidth(1024);
                hardcopyProperties_0.setCurrentResolutionHeight(494);

                scene_0.resetCamera();
                CurrentView currentView_0 = scene_0.getCurrentView();
                currentView_0.setInput(new DoubleVector(
                                new double[] { 1.552230566304047E-14, -8.213661445433118E-9, 0.07000000000000002 }),
                                new DoubleVector(new double[] { 1.552230566304047E-14, -8.213661445433118E-9,
                                                0.47714309349357603 }),
                                new DoubleVector(new double[] { 0.0, 1.0, 0.0 }), 0.10628567536481787, 1, 30.0);

                scene_0.setViewOrientation(new DoubleVector(new double[] { 0.0, 1.0, 0.0 }),
                                new DoubleVector(new double[] { 0.0, 0.0, 1.0 }));
                scene_0.close();

                scene_0.setPresentationName(name);
                scene_0.setBackgroundColorMode(BackgroundColorMode.SOLID);
                LogoAnnotation logoAnnotation_0 = ((LogoAnnotation) simulation.getAnnotationManager()
                                .getObject("Logo"));
                scene_0.getAnnotationPropManager().removePropsForAnnotations(logoAnnotation_0);
                PhysicalTimeAnnotation physicalTimeAnnotation_0 = ((PhysicalTimeAnnotation) simulation
                                .getAnnotationManager().getObject("Solution Time"));
                PhysicalTimeAnnotationProp physicalTimeAnnotationProp_0 = (PhysicalTimeAnnotationProp) scene_0
                                .getAnnotationPropManager().createPropForAnnotation(physicalTimeAnnotation_0);

                sceneUpdate_0.getUpdateModeOption().setSelected(StarUpdateModeOption.Type.DELTATIME);
                DeltaTimeUpdateFrequency deltaTimeUpdateFrequency_0 = sceneUpdate_0.getDeltaTimeUpdateFrequency();
                Units units_0 = ((Units) simulation.getUnitsManager().getObject("s"));
                deltaTimeUpdateFrequency_0.setDeltaTime("0.02", units_0); // 50 FPS
                sceneUpdate_0.setAnimationFilenameBase(name.replace(' ', '_'));
                sceneUpdate_0.setAnimationFilePath(uid + "_" + name.replace(' ', '_'));
                sceneUpdate_0.setSaveAnimation(true);

                scalarDisplayer_0.getInputParts().setQuery(null);
                Region region_0 = simulation.getRegionManager().getRegion("Fluid");
                Boundary boundary_1 = region_0.getBoundaryManager().getBoundary("Inlet");
                Boundary boundary_2 = region_0.getBoundaryManager().getBoundary("Outlet");
                Boundary boundary_3 = region_0.getBoundaryManager().getBoundary("Symmetry");
                Boundary boundary_4 = region_0.getBoundaryManager().getBoundary("Wall");
                scalarDisplayer_0.getInputParts().setObjects(boundary_1, boundary_2, boundary_3, boundary_4);

                FieldFunction primitiveFieldFunction_3 = ((FieldFunction) simulation.getFieldFunctionManager()
                                .getFunction(fieldFunction));
                scalarDisplayer_0.getScalarDisplayQuantity().setFieldFunction(primitiveFieldFunction_3);
                scalarDisplayer_0.getScalarDisplayQuantity().setAutoRange(AutoRangeMode.NONE);
                scalarDisplayer_0.getScalarDisplayQuantity().setRange(new DoubleVector(new double[] { Min, Max }));
                scalarDisplayer_0.getScalarDisplayQuantity().setClip(ClipMode.NONE);
        }

        private void AddSideViewScene(String name, String fieldFunction, double Min, double Max) {

                Simulation simulation = getActiveSimulation();

                simulation.getSceneManager().createScalarScene("Scalar Scene", "Outline", "Scalar");

                Scene scene_0 = simulation.getSceneManager().getScene("Scalar Scene 1");

                scene_0.initializeAndWait();

                PartDisplayer partDisplayer_0 = ((PartDisplayer) scene_0.getDisplayerManager()
                                .getDisplayer("Outline 1"));

                partDisplayer_0.initialize();

                ScalarDisplayer scalarDisplayer_0 = ((ScalarDisplayer) scene_0.getDisplayerManager()
                                .getDisplayer("Scalar 1"));

                scalarDisplayer_0.initialize();

                Legend legend_0 = scalarDisplayer_0.getLegend();

                BlueRedLookupTable blueRedLookupTable_0 = ((BlueRedLookupTable) simulation.get(LookupTableManager.class)
                                .getObject("blue-red"));

                legend_0.setLookupTable(blueRedLookupTable_0);

                SceneUpdate sceneUpdate_0 = scene_0.getSceneUpdate();

                HardcopyProperties hardcopyProperties_0 = sceneUpdate_0.getHardcopyProperties();

                hardcopyProperties_0.setCurrentResolutionWidth(25);

                hardcopyProperties_0.setCurrentResolutionHeight(25);

                hardcopyProperties_0.setCurrentResolutionWidth(1024);

                hardcopyProperties_0.setCurrentResolutionHeight(494);

                scene_0.resetCamera();

                CurrentView currentView_0 = scene_0.getCurrentView();

                currentView_0.setInput(new DoubleVector(
                                new double[] { 1.552230566304047E-14, -8.213661445433118E-9, 0.07000000000000002 }),
                                new DoubleVector(new double[] { 1.552230566304047E-14, -8.213661445433118E-9,
                                                0.47714309349357603 }),
                                new DoubleVector(new double[] { 0.0, 1.0, 0.0 }), 0.10628567536481787, 1, 30.0);

                scene_0.setViewOrientation(new DoubleVector(new double[] { 0.0, 1.0, 0.0 }),
                                new DoubleVector(new double[] { 0.0, 0.0, 1.0 }));

                scene_0.close();

                scene_0.setPresentationName(name);

                scene_0.setBackgroundColorMode(BackgroundColorMode.SOLID);

                LogoAnnotation logoAnnotation_0 = ((LogoAnnotation) simulation.getAnnotationManager()
                                .getObject("Logo"));

                scene_0.getAnnotationPropManager().removePropsForAnnotations(logoAnnotation_0);

                PhysicalTimeAnnotation physicalTimeAnnotation_0 = ((PhysicalTimeAnnotation) simulation
                                .getAnnotationManager().getObject("Solution Time"));

                PhysicalTimeAnnotationProp physicalTimeAnnotationProp_0 = (PhysicalTimeAnnotationProp) scene_0
                                .getAnnotationPropManager().createPropForAnnotation(physicalTimeAnnotation_0);

                sceneUpdate_0.getUpdateModeOption().setSelected(StarUpdateModeOption.Type.DELTATIME);

                DeltaTimeUpdateFrequency deltaTimeUpdateFrequency_0 = sceneUpdate_0.getDeltaTimeUpdateFrequency();
                Units units_0 = ((Units) simulation.getUnitsManager().getObject("s"));
                deltaTimeUpdateFrequency_0.setDeltaTime("0.02", units_0); // 50 FPS

                sceneUpdate_0.setAnimationFilenameBase(name.replace(' ', '_'));
                sceneUpdate_0.setAnimationFilePath(uid + "_" + name.replace(' ', '_'));
                sceneUpdate_0.setSaveAnimation(true);

                scalarDisplayer_0.getInputParts().setQuery(null);
                PlaneSection planeSection_0 = ((PlaneSection) simulation.getPartManager().getObject("XY Plane"));
                PlaneSection planeSection_1 = ((PlaneSection) simulation.getPartManager().getObject("XZ Plane"));
                PlaneSection planeSection_2 = ((PlaneSection) simulation.getPartManager().getObject("YZ Plane"));
                scalarDisplayer_0.getInputParts().setObjects(planeSection_0, planeSection_1, planeSection_2);

                FieldFunction primitiveFieldFunction_3 = ((FieldFunction) simulation.getFieldFunctionManager()
                                .getFunction(fieldFunction));
                scalarDisplayer_0.getScalarDisplayQuantity().setFieldFunction(primitiveFieldFunction_3);
                scalarDisplayer_0.getScalarDisplayQuantity().setAutoRange(AutoRangeMode.NONE);
                scalarDisplayer_0.getScalarDisplayQuantity().setRange(new DoubleVector(new double[] { Min, Max }));
                scalarDisplayer_0.getScalarDisplayQuantity().setClip(ClipMode.NONE);

        }

        private void swapToUnsteady() {

                if (IsSteady) {
                        IsSteady = false;
                        Simulation simulation = getActiveSimulation();

                        PhysicsContinuum physicsContinuum = ((PhysicsContinuum) simulation.getContinuumManager()
                                        .getContinuum("LES"));
                        SteadyModel steadyModel = physicsContinuum.getModelManager().getModel(SteadyModel.class);

                        physicsContinuum.disableModel(steadyModel);
                        physicsContinuum.enable(ImplicitUnsteadyModel.class);
                }
        }

        private void setTimeStep(double deltat) {
                Simulation simulation = getActiveSimulation();

                PhysicsContinuum physicsContinuum = simulation.getContinuumManager()
                                .createContinuum(PhysicsContinuum.class);

                physicsContinuum.enable(ThreeDimensionalModel.class);

                ImplicitUnsteadySolver implicitUnsteadySolver = ((ImplicitUnsteadySolver) simulation.getSolverManager()
                                .getSolver(ImplicitUnsteadySolver.class));

                implicitUnsteadySolver.getTimeStep().setValue(deltat);

        }

        private void SetAutoSave() {

                Simulation simulation = getActiveSimulation();

                AutoSave autoSave = simulation.getSimulationIterator().getAutoSave();

                autoSave.setSeparator("_At_");
                autoSave.setFormatWidth(6);

                StarUpdate starUpdate = autoSave.getStarUpdate();

                starUpdate.setEnabled(true);
                if (IsSteady) {
                        starUpdate.getUpdateModeOption().setSelected(StarUpdateModeOption.Type.ITERATION);
                        IterationUpdateFrequency iterationUpdateFrequency = starUpdate.getIterationUpdateFrequency();
                        iterationUpdateFrequency.setIterations(5000);
                } else {
                        starUpdate.getUpdateModeOption().setSelected(StarUpdateModeOption.Type.TIMESTEP);
                        TimeStepUpdateFrequency timeStepUpdateFrequency = starUpdate.getTimeStepUpdateFrequency();
                        timeStepUpdateFrequency.setTimeSteps(1000);
                }
                autoSave.setMaxAutosavedFiles(2);
        }

        private void SetTransientStoppingCriteria(double physicalTime) {

                Simulation simulation = getActiveSimulation();

                PhysicalTimeStoppingCriterion physicalTimeStoppingCriterion = ((PhysicalTimeStoppingCriterion) simulation
                                .getSolverStoppingCriterionManager()
                                .getSolverStoppingCriterion("Maximum Physical Time"));

                physicalTimeStoppingCriterion.getMaximumTime().setValue(physicalTime);

                StepStoppingCriterion stepStoppingCriterion = ((StepStoppingCriterion) simulation
                                .getSolverStoppingCriterionManager().getSolverStoppingCriterion("Maximum Steps"));

                stepStoppingCriterion.setIsUsed(false);
        }

        private void CreateGlobalDimensionlessParameter(String name, String definition) {
                Simulation simulation = getActiveSimulation();
                ScalarGlobalParameter sParameter = (ScalarGlobalParameter) simulation.get(GlobalParameterManager.class)
                                .createGlobalParameter(ScalarGlobalParameter.class, name);

                sParameter.getQuantity().setDefinition(definition);
        }

        private void SetMassFlowInletFlowRate(String BoundaryName, String RegionName, double massFlowSi) {

                Simulation simulation = getActiveSimulation();

                Region region = simulation.getRegionManager().getRegion(RegionName);

                Boundary boundary = region.getBoundaryManager().getBoundary(BoundaryName);

                MassFlowRateProfile massFlowRateProfile = boundary.getValues().get(MassFlowRateProfile.class);

                massFlowRateProfile.getMethod(ConstantScalarProfileMethod.class).getQuantity().setValue(massFlowSi);
        }

        private void SetPassiveScalarWallFlux(int scalarInd, String fluxFieldFunc, String FluxDerivativeFieldFunc) {

                Simulation simulation = getActiveSimulation();
                Region region = simulation.getRegionManager().getRegion("Fluid");
                Boundary boundary = region.getBoundaryManager().getBoundary("Wall");

                boundary.getConditions().get(WallPassiveScalarOption.class)
                                .setSelected(WallPassiveScalarOption.Type.SPECIFIED_FLUX);
                boundary.getConditions().get(WallPassiveScalarFluxDerivativeOption.class)
                                .setSelected(WallPassiveScalarFluxDerivativeOption.Type.SPECIFIED);

                PassiveScalarFluxProfile passiveScalarFluxProfile = boundary.getValues()
                                .get(PassiveScalarFluxProfile.class);
                passiveScalarFluxProfile.setMethod(CompositeArrayProfileMethod.class);

                PassiveScalarFluxDerivativeProfile passiveScalarFluxDerivativeProfile = boundary.getValues()
                                .get(PassiveScalarFluxDerivativeProfile.class);
                passiveScalarFluxDerivativeProfile.setMethod(CompositeArrayProfileMethod.class);

                ScalarProfile scalarProfile = passiveScalarFluxProfile.getMethod(CompositeArrayProfileMethod.class)
                                .getProfile(scalarInd);
                scalarProfile.setMethod(FunctionScalarProfileMethod.class);
                UserFieldFunction fluxFieldFunction = ((UserFieldFunction) simulation.getFieldFunctionManager()
                                .getFunction(fluxFieldFunc));
                scalarProfile.getMethod(FunctionScalarProfileMethod.class).setFieldFunction(fluxFieldFunction);

                ScalarProfile scalarProfile_1 = passiveScalarFluxDerivativeProfile
                                .getMethod(CompositeArrayProfileMethod.class).getProfile(scalarInd);
                scalarProfile_1.setMethod(FunctionScalarProfileMethod.class);
                UserFieldFunction FluxDerivativeFieldFunction = ((UserFieldFunction) simulation
                                .getFieldFunctionManager().getFunction(FluxDerivativeFieldFunc));
                scalarProfile_1.getMethod(FunctionScalarProfileMethod.class)
                                .setFieldFunction(FluxDerivativeFieldFunction);
        }

        private void SetPassiveScalarMassSource(int scalarInd, String sourceFieldFunc,
                        String sourceDerivativeFieldFunc) {

                Simulation simulation = getActiveSimulation();

                Region region = simulation.getRegionManager().getRegion("Fluid");

                region.getConditions().get(PassiveScalarUserSourceOption.class)
                                .setSelected(PassiveScalarUserSourceOption.Type.EXPLICIT_DENSITY);

                PassiveScalarUserSource passiveScalarUserSource = region.getValues().get(PassiveScalarUserSource.class);

                passiveScalarUserSource.setMethod(CompositeArrayProfileMethod.class);

                ScalarProfile scalarProfile = passiveScalarUserSource.getMethod(CompositeArrayProfileMethod.class)
                                .getProfile(scalarInd);
                scalarProfile.setMethod(FunctionScalarProfileMethod.class);
                UserFieldFunction userFieldFunction = ((UserFieldFunction) simulation.getFieldFunctionManager()
                                .getFunction(sourceFieldFunc));
                scalarProfile.getMethod(FunctionScalarProfileMethod.class).setFieldFunction(userFieldFunction);
                PassiveScalarUserSourceDerivative passiveScalarUserSourceDerivative = region.getValues()
                                .get(PassiveScalarUserSourceDerivative.class);

                passiveScalarUserSourceDerivative.setMethod(CompositeArrayProfileMethod.class);

                ScalarProfile scalarProfile_1 = passiveScalarUserSourceDerivative
                                .getMethod(CompositeArrayProfileMethod.class).getProfile(scalarInd);
                scalarProfile_1.setMethod(FunctionScalarProfileMethod.class);
                UserFieldFunction userFieldFunction_1 = ((UserFieldFunction) simulation.getFieldFunctionManager()
                                .getFunction(sourceDerivativeFieldFunc));
                scalarProfile_1.getMethod(FunctionScalarProfileMethod.class).setFieldFunction(userFieldFunction_1);
        }

        private void setMinWallDistance(double minWallDistance) {

                Simulation simulation = getActiveSimulation();

                PhysicsContinuum physicsContinuum = ((PhysicsContinuum) simulation.getContinuumManager()
                                .getContinuum("LES"));

                physicsContinuum.getReferenceValues().get(MinimumAllowableWallDistance.class).setValue(minWallDistance);

                Units units = ((Units) simulation.getUnitsManager().getObject("m"));

                physicsContinuum.getReferenceValues().get(MinimumAllowableWallDistance.class).setUnits(units);
        }

        private void SetAmgMaxIterations(int maxIter) {

                Simulation simulation = getActiveSimulation();

                SegregatedFlowSolver segregatedFlowSolver = ((SegregatedFlowSolver) simulation.getSolverManager()
                                .getSolver(SegregatedFlowSolver.class));

                PressureSolver pressureSolver = segregatedFlowSolver.getPressureSolver();

                AMGLinearSolver aMGLinearSolver_pressure = pressureSolver.getAMGLinearSolver();

                aMGLinearSolver_pressure.setMaxCycles(maxIter);

                VelocitySolver velocitySolver = segregatedFlowSolver.getVelocitySolver();

                AMGLinearSolver aMGLinearSolver_velocity = velocitySolver.getAMGLinearSolver();

                aMGLinearSolver_velocity.setMaxCycles(maxIter);
        }

        private void setPorosityFieldFunction(String fieldFunction) {

                Simulation simulation = getActiveSimulation();

                Region region = simulation.getRegionManager().getRegion("Fluid");

                VolumePorosityProfile volumePorosityProfile = region.getValues().get(VolumePorosityProfile.class);

                volumePorosityProfile.setMethod(FunctionScalarProfileMethod.class);

                UserFieldFunction userFieldFunction = ((UserFieldFunction) simulation.getFieldFunctionManager()
                                .getFunction(fieldFunction));

                volumePorosityProfile.getMethod(FunctionScalarProfileMethod.class).setFieldFunction(userFieldFunction);
        }
}
