# Link to this file: https://gitlab.gbar.dtu.dk/jroben/threadedPipeScaling/raw/master/src/ReactiveLes/LesReactive.sh
concentrationNa2SO4=0.00204
concentrationBaCl2=0.000972
TurbulentSchmidtNumber=0.75
Temperature=60
k1=1e8
kn=0.01

rm *.java
rm run*.slurm
rm libuser.so
mkdir -p Results
wget https://gitlab.gbar.dtu.dk/jroben/threadedPipeScaling/raw/master/src/ReactiveLes/PipeReactiveStudyLes.java
wget https://gitlab.gbar.dtu.dk/jroben/threadedPipeScaling/raw/master/src/ReactiveLes/runUnix3.slurm
wget https://gitlab.gbar.dtu.dk/jroben/threadedPipeScaling/raw/master/src/ReactiveLes/runXeon40.slurm
wget https://gitlab.gbar.dtu.dk/jroben/threadedPipeScaling/raw/master/src/libuser.so

cp libuser.so Results/


sed "s/__k1__/$k1/" -i PipeReactiveStudyLes.java
sed "s/__kn__/$kn/" -i PipeReactiveStudyLes.java
sed "s/__concentrationNa2SO4__/$concentrationNa2SO4/" -i PipeReactiveStudyLes.java
sed "s/__concentrationBaCl2__/$concentrationBaCl2/" -i PipeReactiveStudyLes.java
sed "s/__TurbulentSchmidtNumber__/$TurbulentSchmidtNumber/" -i PipeReactiveStudyLes.java
sed "s/__Temperature__/$Temperature/" -i PipeReactiveStudyLes.java
